#!/bin/python3

import asyncio
import concurrent.futures
import contextlib
import math
import os
import random
import sys
import threading
import time

import aiohttp
import requests

""" Constants """

URL = "http://localhost:5000/work"
machine_names = ['us/0', 'asia/1', 'asia/0', 'us/1', 'emea/0']
machines = [f'{URL}/{m}' for m in machine_names]
# The bias is used to choose a machine more frequently if their recorded response is lower
BIAS_MIN = 1
BIAS_MAX = 3
# The number of requests to batch together in strategy 4
SLICE_SIZE = 150
# Print style
PRETTY_PRINT = True


""" Helpers """

@contextlib.contextmanager
def report_time(name, no_requests=1):
    start = time.time()
    yield
    dur = time.time() - start

    print_string = "Execution time:  `%s` - %d REQ in %.2fs - %.2f REQ/s" if PRETTY_PRINT else "E,%s,%d,%.5f,%.5f"
    print(print_string % (name, no_requests, dur, no_requests / dur))


def print_response_times(response_times=[]):
    if len(response_times) == 0:
        return

    min_res = min(response_times)
    avg_res = sum(response_times) / len(response_times)
    max_res = max(response_times)
    std_res = math.sqrt(sum(
        [(avg_res - r) ** 2 for r in response_times]
    ) / len(response_times))

    print_string = "Response times:  %d (min) - %.2f (avg) - %d (max) - %.2f (std)" if PRETTY_PRINT else "R,%.5f,%.5f,%.5f,%.5f"
    print(print_string % (min_res, avg_res, max_res, std_res))


def average_machine_metric(responses, metric='response_time'):
    if metric != 'response_time' and metric != 'work_time':
        metric = 'response_time'

    measured = [(0, 0)] * 5
    for r in responses:
        idx = machine_names.index(f"{r['region']}/{int(r['machine'][8:])}")
        measured[idx] = (measured[idx][0] + 1, measured[idx][1] + r[metric])

    return [t[1] / (t[0] or 1) for t in measured]


def summarise_responses(responses):
    print_string = "Summarizing %d responses." if PRETTY_PRINT else "S,%d"
    print(print_string % (len(responses)))

    print_response_times([res['response_time'] for res in responses])

    averaged_machines = average_machine_metric(responses)

    if PRETTY_PRINT:
        print("Machine average: " + " ".join(
            ["%.2f (%s)" % (averaged_machines[i], machine_names[i]) for i in range(len(machines))]
        ))
    else:
        print("M," + ",".join(["%.5f" % m for m in averaged_machines]))


def bias_metric(recorded_values):
    t = [max(recorded_values) - r for r in recorded_values]
    t = [int((BIAS_MAX - BIAS_MIN) / max(t) * i + BIAS_MIN) for i in t]  if max(t) != 0 else [i for i in range(len(t))]
    z = [(i, t[i]) for i in range(len(t))]
    l = [[i[0]] * int(i[1]) for i in z]
    l = [j for i in l for j in i]
    return l


""" Strategies """


# Strategy 0

def strategy_0(no_requests):
    """
        Strategy 0: concurrent threads in a pool of workers of size determined by the CPU
        The machines are chosen sequentially, to make sure the work is distributed evenly.
    """
    responses = []

    with report_time("strategy_0", no_requests):
        with concurrent.futures.ThreadPoolExecutor(max_workers=(os.cpu_count() + 1) * 5) as pool:
            responses = list(pool.map(
                requests.get, [machines[i % 5] for i in range(no_requests)]
            ))

    summarise_responses([res.json() for res in responses])


# Strategy 1

def strategy_1_helper(no_requests, responses, i):
    while no_requests:
        res = requests.get(machines[i])
        responses.append(res)
        no_requests = no_requests - 1


def strategy_1(no_requests):
    """
        Strategy 1: one thread per target machine, requests are sequential for each thread
    """
    responses = []

    with report_time("strategy_1", no_requests):
        threads = [threading.Thread(
            target=strategy_1_helper, args=(no_requests // 5, responses, i)) for i in range(5)]

        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()

    summarise_responses([res.json() for res in responses])


# Strategy 2

async def strategy_2_req(session, requested, responses):
    m = min(requested)
    i = requested.index(m)
    requested[i] = m + 1
    async with session.request(method="GET", url=machines[i]) as response:
        response = await response.json()
        responses.append(response)


async def strategy_2_helper(requested, responses, no_requests) -> None:
    async with aiohttp.ClientSession() as session:
        tasks = [
            strategy_2_req(session, requested, responses) for i in range(no_requests)
        ]
        await asyncio.gather(*tasks)


def strategy_2(no_requests):
    '''
        Strategy 2: async io with concurrent threads and connection session sharing.
        This reduces connection overhead.
    '''
    requested = [0] * len(machines)
    responses = []

    with report_time("strategy_2", no_requests):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(
            strategy_2_helper(requested, responses, no_requests)
        )

    summarise_responses(responses)


# Strategy 3

def strategy_3(no_requests):
    """
        Strategy 3: use a first small set of 5 to benchmark current machine state and choose
        then number of following async request in a biased manner based on best performance.
    """
    requested = [0] * len(machines)
    init_responses = []
    responses = []

    with report_time("strategy_3", no_requests):
        with concurrent.futures.ThreadPoolExecutor() as pool:
            responses = list(pool.map(
                requests.get, [machines[i % 5] for i in range(len(machines))]
            ))

        init_responses = [res.json() for res in responses]
        biased = bias_metric(average_machine_metric(init_responses))

        with concurrent.futures.ThreadPoolExecutor(max_workers=(os.cpu_count() + 1) * 5) as pool:
            responses = list(pool.map(
                requests.get, [machines[random.choice(biased)] for _ in range(no_requests - len(machines))]
            ))

    responses = init_responses + [r.json() for r in responses]
    summarise_responses(responses)


# Strategy 4

def strategy_4(no_requests, slice_size=SLICE_SIZE, metric='response_time'):
    """
        Strategy 4: similar to strategy 3, it chooses current load based on a subset of the
        previous responses, biased in the direction of the best values of the chosen metric.

        However, all the requests are batched into slices of SLICE_SIZE or less.
    """
    biased = [i for i in range(len(machines))]
    responses = []

    if PRETTY_PRINT: print("Using metric: %s" % metric)

    with report_time("strategy_4", no_requests):
        slices = ([slice_size] * (no_requests // slice_size)) + (
            [no_requests % slice_size] if no_requests % slice_size != 0 else []
        )

        for slice in slices:
            with concurrent.futures.ThreadPoolExecutor(max_workers=(os.cpu_count() + 1) * 5) as pool:
                current_responses = list(pool.map(
                    requests.get, [machines[random.choice(biased)] for _ in range(slice)]
                ))

            json_responses = [res.json() for res in current_responses]
            biased = bias_metric(average_machine_metric(json_responses, metric))
            responses = responses + json_responses

    summarise_responses(responses)


""" Execution """

def main():
    if not os.getenv("N"):
        sys.exit("Missing number of requests. Set environment variable `N` or pass as an argument. Exiting")

    if os.getenv('HOST'):
        global URL
        URL = f"http://{os.getenv('HOST')}:5000/work"
        global machines
        machines = [f'{URL}/{m}' for m in machine_names]

    if len(sys.argv) >= 2:
        global PRETTY_PRINT
        PRETTY_PRINT = not (sys.argv[1] == 'False')

    NO_REQ = int(os.getenv("N"))

    if NO_REQ < 50:
        strategy_1(NO_REQ)
    elif NO_REQ < 350:
        strategy_2(NO_REQ)
    elif NO_REQ < 750:
        strategy_4(NO_REQ)
    elif NO_REQ < 1000:
        strategy_0(NO_REQ)
    else:
        strategy_3(NO_REQ)

main()
