FROM python:3.8.5-slim

WORKDIR /app

COPY main.py .

COPY requirements.txt .

RUN pip install -r requirements.txt

ENV N=100

CMD ["python", "main.py"]
