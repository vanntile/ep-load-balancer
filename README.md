# ep-load-balancer

Assignment that covers implementing at least 5 load balancing strategies on a
containerised architecture running on Heroku.

## Start developing

```sh
bash deploy_docker.sh username
bash run.sh username
```

## How to run

Set the number of requests, `N` in the `docker-compose.yml`.

```sh
docker-compose up --build
```
