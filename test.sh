#!/bin/sh

sizes=(20 50 100 200 500 750 1000 2000)

for size in ${sizes[@]}; do
  for i in {1..10}; do
    res=`python main.py $size False`
    echo "$res" | grep "E," >> "test-$size-E.csv"
    echo "$res" | grep "S," >> "test-$size-S.csv"
    echo "$res" | grep "R," >> "test-$size-R.csv"
    echo "$res" | grep "M," >> "test-$size-M.csv"
    sleep 10
  done
done
